<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Francisco Giraldo</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('recursos/css/estilo.css') ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('recursos/css/tipTip.css') ?>" />

	<script type="text/javascript" src="<?php echo base_url('recursos/js/jquery-1.7.2.min.js') ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('recursos/js/jquery.tipTip.minified.js') ?>"></script>	

	<script type="text/javascript">
	$(function(){
		$(".tooltip").tipTip({maxWidth: "auto", edgeOffset: 10, defaultPosition: "top"});
	});	
	</script>	
</head>
<body>

<h1><a href="<?php echo base_url('trabajos/empresa') ?>">Control de Trabajos</a></h1>	
<p><a href="<?php echo base_url('ingreso/salir') ?>" title="Salir">Salir</a></p>