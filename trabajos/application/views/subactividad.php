<h2>Subactividad: <?php echo $subactividad->nombre ?></h2>

<div>
	<a href="<?php echo base_url('subactividades/listar/'.$subactividad->actividad_id) ?>" class="tooltip" title="Devolverse a la Subactividad">
		<img src="<?php echo base_url('recursos/imagenes/atras.png') ?>">
	</a>
</div>

<div id="" class="menu">
	<a href="<?php echo base_url('cambios/crear/'.$subactividad->id) ?>">Actualizar subactividad</a>
</div>

<div>
	<table>
		<caption>Subactividad</caption>
		<thead>
			<tr>
				<th>Nombre</th>
				<th>Descripción</th>
				<th>Autor</th>
				<th>Estado</th>
				<th>Creado</th>
				<th>Modificado</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td><?php echo $subactividad->nombre ?></td>
				<td><?php echo $subactividad->descripcion ?></td>
				<td><?php echo $autor->nombre ?></td>
				<td><?php echo $subactividad->estado ?></td>
				<td><?php echo $subactividad->fecha_creacion ?></td>
				<td><?php echo $subactividad->fecha_ultima_modificacion ?></td>
			</tr>
		</tbody>
	</table>
</div>
	

<h3>Cambios</h3>


<div>
	
	<?php if (count($cambios) == 0): ?>

		<p>No hay cambios registrados</p>

	<?php else: ?>

		<table>
			<caption>Cambios</caption>
			<thead>
				<tr>
					<th>Descripción</th>
					<th>Autor</th>
					<th>Creado</th>		
				</tr>
			</thead>
			<tbody>
				<?php foreach ($cambios as $cambio): ?>
				<tr>
					<td><?php echo $cambio->descripcion ?></td>
					<td><?php echo $cambio->autor ?></td>
					<td><?php echo $cambio->fecha ?></td>				
				</tr>
				<?php endforeach ?>
			</tbody>
		</table>

	<?php endif ?>

</div>