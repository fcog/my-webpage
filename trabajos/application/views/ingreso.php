<!DOCTYPE html>
<html lang="en">

	<head>
		<meta charset="utf-8">
	    <link rel="stylesheet" type="text/css" href="<?php echo base_url('recursos/css/estilo.css') ?>" />
		<title>Ingreso</title>
	</head>

	<body>
		
		<fieldset id="ingreso">
			<legend>Ingreso</legend>
			<form method="POST" action="<?php echo base_url('ingreso') ?>">
				<?php if (isset($error)) echo $error ?>
				<p><label>Usuario: </label><input type="text" id="usuario" name="usuario"></p>
				<p><label>Clave: </label><input type="password" id="password" name="password"></p>
				<p><input type="submit" id="ingresar" name="ingresar" value="Ingresar"></p>
			</form>
		</fieldset>

	</body>

</html>