<h2>Trabajo: <?php echo $trabajo->nombre ?></h2>

<div>
	<a href="<?php echo base_url('trabajos/empresa/') ?>" class="tooltip" title="Ir a Trabajos">
		<img src="<?php echo base_url('recursos/imagenes/atras.png') ?>">
	</a>
</div>

<div id="" class="menu">
	<?php if ($_SESSION['usuario_rol'] == "admin"): ?>
		<a href="<?php echo base_url('actividades/crear/'.$trabajo->id) ?>">Crear Nueva Actividad</a>
	<?php endif ?>
</div><!-- / -->

<div>
	
	<?php if (count($actividades) == 0): ?>

		<p>No hay actividades registradas</p>

	<?php else: ?>

		<table>
			<caption>Actividades</caption>
			<thead>
				<tr>
					<th>Nombre</th>
					<th>Descripción</th>
					<th>Estado</th>
					<th>Porcentaje realizado</th>
					<th>Creado</th>
					<th>Modificado</th>					
				</tr>
			</thead>
			<tbody>
				<?php foreach ($actividades as $actividad): ?>
				<tr>
					<td><a href="<?php echo base_url('subactividades/listar/'.$actividad['id']) ?>"><?php echo $actividad['nombre'] ?></a></td>
					<td><?php echo $actividad['descripcion'] ?></td>
					<td><?php echo $actividad['estado'] ?></td>
					<td><?php echo $actividad['porcentaje'] ?>%</td>
					<td><?php echo $actividad['fecha_creacion'] ?></td>
					<td><?php echo $actividad['fecha_ultima_modificacion'] ?></td>					
				</tr>
				<?php endforeach ?>
			</tbody>
		</table>

	<?php endif ?>

</div>