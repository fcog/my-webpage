<h2>Actividad: <?php echo $actividad->nombre ?></h2>

<div>
	<a href="<?php echo base_url('actividades/listar/'.$actividad->trabajo_id) ?>" class="tooltip" title="Devolverse a la Actividad">
		<img src="<?php echo base_url('recursos/imagenes/atras.png') ?>">
	</a>
</div>

<div id="" class="menu">
	<a href="<?php echo base_url('subactividades/crear/'.$actividad->id) ?>">Crear Nueva Subactividad</a>
</div>

<div>

	<?php if (count($subactividades) == 0): ?>

		<p>No hay subactividades registradas</p>

	<?php else: ?>

		<table>
			<caption>Subactividades</caption>
			<thead>
				<tr>
					<th>Nombre</th>
					<th>Descripción</th>
					<th>Autor</th>
					<th>Estado</th>
					<th>Creado</th>
					<th>Modificado</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($subactividades as $subactividad): ?>
				<tr>
					<td><a href="<?php echo base_url('subactividades/ver/'.$subactividad->id) ?>"><?php echo $subactividad->nombre ?></a></td>
					<td><?php echo $subactividad->descripcion ?></td>
					<td><?php echo $subactividad->autor ?></td>
					<td><?php echo $subactividad->estado ?></td>
					<td><?php echo $subactividad->fecha_creacion ?></td>
					<td><?php echo $subactividad->fecha_ultima_modificacion ?></td>
				</tr>
				<?php endforeach ?>
			</tbody>
		</table>

	<?php endif ?>

</div>