<script src="http://js.nicedit.com/nicEdit-latest.js" type="text/javascript"></script>
<script type="text/javascript">bkLib.onDomLoaded(nicEditors.allTextAreas);</script>	

<div>
	<a href="<?php echo base_url('subactividades/ver/'.$subactividad->id) ?>" class="tooltip" title="Devolverse a la subactividad">
		<img src="<?php echo base_url('recursos/imagenes/atras.png') ?>">
	</a>
</div>

<fieldset>
	<legend>Crear cambio para <strong><?php echo $subactividad->nombre ?></strong></legend>
	<form action="<?php echo base_url('cambios/crear/'.$subactividad->id) ?>" method="POST" accept-charset="utf-8">
		<p>
			<label>Estado de la subactividad: 
				<select name="estado" id="estado">
					<option value="pendiente" <?php if ($subactividad->estado == 'pendiente') echo "selected=selected" ?>>Pendiente</option>
					<option value="incompleto"<?php if ($subactividad->estado == 'incompleto') echo "selected=selected" ?>>Incompleta</option>
					<option value="revision"<?php if ($subactividad->estado == 'revision') echo "selected=selected" ?>>Revisión</option>
					<option value="terminado"<?php if ($subactividad->estado == 'terminado') echo "selected=selected" ?>>Terminada</option>
				</select>
			<label>
		</p>
		<p><label>Descripción:</label></p>
		<p><textarea id="descripcion" name="descripcion"></textarea></p>
		<p><input type="submit" name="guardar" id="guardar" value="Guardar"></p>
	</form>
</fieldset>