<h2>Trabajos para <?php echo $empresa ?></h2>

<div>
	<table>
		<thead>
			<tr>
				<?php if ($_SESSION['usuario_rol'] == 'admin'): ?><th>Empresa</th><?php endif ?>
				<th>Nombre</th>
				<th>Estado</th>
				<th>Porcentaje realizado</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($trabajos as $trabajo): ?>
			<tr>
				<?php if ($_SESSION['usuario_rol'] == 'admin'): ?><td><?php echo $trabajo['empresa'] ?></td><?php endif ?>
				<td><a href="<?php echo base_url('actividades/listar/'.$trabajo['id']) ?>"><?php echo $trabajo['nombre'] ?></a></td>
				<td><?php echo $trabajo['estado'] ?></td>
				<td><?php echo $trabajo['porcentaje'] ?>%</td>
			</tr>
			<?php endforeach ?>
		</tbody>
	</table>	
</div>