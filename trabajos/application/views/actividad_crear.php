<div>
	<a href="<?php echo base_url('trabajos/empresa/') ?>" class="tooltip" title="Ir a Trabajos">
		<img src="<?php echo base_url('recursos/imagenes/atras.png') ?>">
	</a>
</div>

<fieldset>
	<legend>Crear Actividad para <strong><?php echo $trabajo->nombre ?></strong></legend>
	<form action="<?php echo base_url('actividades/crear/'.$trabajo->id) ?>" method="POST" accept-charset="utf-8">
		<p><label>Nombre: <input type="text" name="nombre" id="nombre"></label></p>
		<p><label>Descripción:</label></p>
		<p><textarea id="descripcion" name="descripcion"></textarea></p>
		<p><input type="submit" name="guardar" id="guardar" value="Guardar"></p>
	</form>
</fieldset>