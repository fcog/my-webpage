<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cambios extends MY_Controller 
{
	function __construct()
	{
		parent::__construct();
		
		acceso();
	}
	
	function crear($subactividad_id)
	{
		if (isset($_POST['guardar'])) 
		{
			$estado      = $_POST['estado'];
			$descripcion = $_POST['descripcion'];

			$cambio = New Cambio_Model;		

			$cambio->crear($estado, $descripcion, $subactividad_id, $_SESSION['usuario_id']);

			redirect('subactividades/ver/'.$subactividad_id);	
		}

		$subactividad = New Subactividad_Model;	

		$subactividad->cargar($subactividad_id);

		$data['subactividad'] = $subactividad;

		$this->master_view('cambio_crear', $data);
	}

}
