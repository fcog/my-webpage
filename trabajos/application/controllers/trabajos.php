<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Trabajos extends MY_Controller 
{
	function __construct()
	{
		parent::__construct();
		
		acceso();
	}
		
	function empresa()
	{
		$usuario = New Usuario_Model;

		$empresa = New Empresa_Model;

		$trabajos = New Trabajo_Model;		

		$usuario->cargar($_SESSION['usuario_id']);

		$empresa->cargar($usuario->empresa_id);		

		$data['empresa'] = $empresa->nombre;

		$data['trabajos'] = $trabajos->listar($empresa);

		$this->master_view('trabajos', $data);
	}


}
