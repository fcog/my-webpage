<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Actividades extends MY_Controller 
{
	function __construct()
	{
		parent::__construct();
		
		acceso();
	}
	
	function listar($trabajo_id)
	{
		$trabajo = New Trabajo_Model;	

		$trabajo->cargar($trabajo_id);

		$data['trabajo'] = $trabajo;

		$data['actividades'] = $trabajo->listar_actividades();

		$this->master_view('actividades', $data);
	}


	function crear($trabajo_id)
	{
		if (isset($_POST['guardar'])) 
		{
			$nombre      = $_POST['nombre'];
			$descripcion = $_POST['descripcion'];

			$actividad = New Actividad_Model;		

			$actividad->crear($nombre, $descripcion, $trabajo_id);

			redirect('actividades/listar/'.$trabajo_id);	
		}

		$trabajo = New Trabajo_Model;	

		$trabajo->cargar($trabajo_id);

		$data['trabajo'] = $trabajo;

		$this->master_view('actividad_crear', $data);
	}

}
