<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Subactividades extends MY_Controller 
{
	function __construct()
	{
		parent::__construct();
		
		acceso();
	}
	
	function listar($actividad_id)
	{
		$actividad = new Actividad_Model;

		$actividad->cargar($actividad_id);

		$data['actividad'] = $actividad;

		$data['subactividades'] = $actividad->listar_subactividades();

		$this->master_view('subactividades', $data);		
	}


	function crear($actividad_id)
	{
		if (isset($_POST['guardar'])) 
		{
			$nombre      = $_POST['nombre'];
			$descripcion = $_POST['descripcion'];

			$subactividad = New Subactividad_Model;		

			$subactividad->crear($nombre, $descripcion, $actividad_id, $_SESSION['usuario_id']);

			redirect('subactividades/listar/'.$actividad_id);	
		}

		$actividad = New Actividad_Model;	

		$actividad->cargar($actividad_id);

		$data['actividad'] = $actividad;

		$this->master_view('subactividad_crear', $data);
	}


	function ver($subactividad_id)
	{
		$subactividad = new Subactividad_Model;

		$autor = new Usuario_Model;

		$subactividad->cargar($subactividad_id);

		$autor->cargar($subactividad->usuario_id);

		$data['subactividad'] = $subactividad;

		$data['autor'] = $autor;

		$data['cambios'] = $subactividad->listar_cambios();

		$this->master_view('subactividad', $data);		
	}

}
