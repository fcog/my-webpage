<?php

class Actividad_Model extends CI_Model 
{
	public $id                        = '';
	public $nombre                    = '';
	public $descripcion               = '';
	public $fecha_creacion            = '';
	public $fecha_ultima_modificacion = '';	
	public $estado                    = '';
	public $trabajo_id                = '';	

	function crear($nombre, $descripcion, $trabajo_id)
	{
		$fecha = date("Y-m-d");

		$nombre = addslashes($nombre);

		$descripcion = addslashes($descripcion);

		$this->db->query("INSERT INTO actividades
							SET 
								nombre                    = '$nombre',
								descripcion               = '$descripcion',
								fecha_creacion            = '$fecha',
								fecha_ultima_modificacion = '$fecha',
								trabajos_id                = $trabajo_id
						");

		$this->id                        = $this->db->insert_id();
		$this->nombre                    = $nombre;
		$this->descripcion               = $descripcion;
		$this->fecha_creacion            = $fecha;
		$this->fecha_ultima_modificacion = $fecha;	
		$this->estado                    = 'pendiente';	
		$this->trabajo_id                = $trabajo_id;		
	}


	function cargar($id)
	{
		$query = $this->db->query("SELECT * FROM actividades WHERE id = $id");

		$datos = $query->row();
		
		$this->id                        = $datos->id;
		$this->nombre                    = $datos->nombre;
		$this->descripcion               = $datos->descripcion;
		$this->fecha_creacion            = $datos->fecha_creacion;
		$this->fecha_ultima_modificacion = $datos->fecha_ultima_modificacion;	
		$this->estado                    = $datos->estado;	
		$this->trabajo_id                = $datos->trabajos_id;			
	}


	function listar_subactividades()
	{
		$query = $this->db->query("SELECT a.*, u.nombre as autor
									FROM subactividades a
									JOIN usuarios u ON u.id = a.usuarios_id
									WHERE actividades_id = $this->id");

		return $query->result();
	}

}
?>