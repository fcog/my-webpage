<?php

class Subactividad_Model extends CI_Model 
{
	public $id                        = '';
	public $nombre                    = '';
	public $descripcion               = '';
	public $fecha_creacion            = '';
	public $fecha_ultima_modificacion = '';	
	public $estado                    = '';
	public $actividad_id              = '';	
	public $usuario_id                = '';	

	function crear($nombre, $descripcion, $actividad_id, $usuario_id)
	{
		$fecha = date("Y-m-d");

		$nombre = addslashes($nombre);

		$descripcion = addslashes($descripcion);

		$this->db->query("INSERT INTO subactividades
							SET 
								nombre                    = '$nombre',
								descripcion               = '$descripcion',
								fecha_creacion            = '$fecha',
								fecha_ultima_modificacion = '$fecha',
								actividades_id            = $actividad_id,
								usuarios_id               = $usuario_id
						");

		$this->id                        = $this->db->insert_id();
		$this->nombre                    = $nombre;
		$this->descripcion               = $descripcion;
		$this->fecha_creacion            = $fecha;
		$this->fecha_ultima_modificacion = $fecha;	
		$this->estado                    = 'pendiente';	
		$this->actividad_id              = $actividad_id;		
		$this->usuario_id                = $usuario_id;	

		//enviar notificacion por email si el usuario no soy yo

		if ($_SESSION['usuario_rol'] != 'admin') 
		{
			$this->load->helper('email');

			//sacar info para email
			$autor = new Usuario_Model;
			$autor->cargar($this->usuario_id );

			$empresa = new Empresa_Model;
			$empresa->cargar($autor->empresa_id);

			$actividad = new Actividad_Model;
			$actividad->cargar($this->actividad_id);

			$mensaje="<h2>Nueva subactividad</h2>
					  <p>Empresa: ".$empresa->nombre."</p>
					  <p>Autor: ".$autor->nombre."</p>	
					  <p>Subactividad: ".$this->nombre."</p>
					  <p>Actividad: ".$actividad->nombre."</p>
					  <p>Descripción:</p>
					  <p>".$this->descripcion."</p>";

			$recipient[] = array('fcog111@gmail.com', 'Francisco');

			enviar_email($mensaje, 'Nueva subactividad', $recipient);			
		}

	}


	function cargar($id)
	{
		$query = $this->db->query("SELECT * FROM subactividades WHERE id = $id");

		$datos = $query->row();
		
		$this->id                        = $datos->id;
		$this->nombre                    = $datos->nombre;
		$this->descripcion               = $datos->descripcion;
		$this->fecha_creacion            = $datos->fecha_creacion;
		$this->fecha_ultima_modificacion = $datos->fecha_ultima_modificacion;	
		$this->estado                    = $datos->estado;	
		$this->actividad_id              = $datos->actividades_id;			
		$this->usuario_id                = $datos->usuarios_id;			
	}


	function listar_cambios()
	{
		$query = $this->db->query("SELECT c.*, u.nombre as autor
									FROM cambios c
									JOIN usuarios u ON u.id = c.usuarios_id
									WHERE subactividades_id = $this->id");

		return $query->result();		
	}

}
?>