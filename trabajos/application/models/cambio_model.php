<?php

class Cambio_Model extends CI_Model 
{
	public $id              = '';
	public $descripcion     = '';
	public $fecha           = '';
	public $subactividad_id = '';	
	public $usuario_id      = '';	


	function crear($estado, $descripcion, $subactividad_id, $usuario_id)
	{
		$this->db->trans_start();

		$fecha = date("Y-m-d h:i:s");

		$descripcion = addslashes($descripcion);

		$this->db->query("INSERT INTO cambios
							SET 
								descripcion       = '$descripcion',
								fecha             = '$fecha',
								subactividades_id = $subactividad_id,
								usuarios_id       = $usuario_id
						");

		$this->id              = $this->db->insert_id();
		$this->descripcion     = $descripcion;
		$this->fecha           = $fecha;	
		$this->subactividad_id = $subactividad_id;		
		$this->usuario_id      = $usuario_id;	

		$this->db->query("UPDATE 
								subactividades
							SET 
								estado = '$estado',
								fecha_ultima_modificacion = '$fecha'
							WHERE 
								id = $subactividad_id
						");		

		//averiguar id de la actividad
		$subactividad = new Subactividad_Model;

		$subactividad->cargar($subactividad_id);

		//------------------------------------------------------------------------------------------------------------------
		//averiguar si todas las subactividades terminaron para actualizar el estado de la actividad a terminado
		if ($estado == 'terminado') 
		{
			$query = $this->db->query("SELECT estado FROM subactividades WHERE actividades_id = $subactividad->actividad_id");

			$subactividades_terminadas = TRUE;

			foreach ($query->result() as $subactividad_temp)
			{
				if ($subactividad_temp->estado != 'terminado') 
				{
					$subactividades_terminadas = FALSE;
				}
			}
		}

		$estado = (
					!isset($subactividades_terminadas) OR 
					(isset($subactividades_terminadas) AND $subactividades_terminadas == FALSE)
				  ) 
				  ? 'incompleto' : 'terminado';

		$this->db->query("UPDATE 
								actividades
							SET 
								estado = '$estado',
								fecha_ultima_modificacion = '$fecha'
							WHERE 
								id = $subactividad->actividad_id
						");

		//------------------------------------------------------------------------------------------------------------------
		//averiguar id del trabajo
		$actividad = new Actividad_Model;

		$actividad->cargar($subactividad->actividad_id);


		//averiguar si todas las actividades terminaron para actualizar el estado del trabajo a terminado
		if ($estado == 'terminado') 
		{
			$query = $this->db->query("SELECT estado FROM actividades WHERE trabajos_id = $actividad->trabajo_id");

			$actividades_terminadas = TRUE;

			foreach ($query->result() as $actividad_temp)
			{
				if ($actividad_temp->estado != 'terminado') 
				{
					$actividades_terminadas = FALSE;
				}
			}
		}

		$estado = (
					!isset($actividades_terminadas) OR
					(isset($actividades_terminadas) AND $actividades_terminadas == FALSE)
				  ) 
		          ? 'incompleto' : 'terminado';

		$this->db->query("UPDATE 
								trabajos
							SET 
								estado = '$estado',
								fecha_ultima_modificacion = '$fecha'
							WHERE 
								id = $actividad->trabajo_id
						");	

		$this->db->trans_complete();
		
		if ($this->db->trans_status() === FALSE)
		{
		    die("Sucedió un error guardando los datos");
		}									
	}	


	function cargar($id)
	{
		$query = $this->db->query("SELECT * FROM subactividades WHERE id = $id");

		$datos = $query->row();
		
		$this->id              = $datos->id;
		$this->descripcion     = $datos->descripcion;
		$this->fecha           = $datos->fecha;
		$this->subactividad_id = $datos->subactividades_id;			
		$this->usuario_id      = $datos->usuarios_id;			
	}

}
?>