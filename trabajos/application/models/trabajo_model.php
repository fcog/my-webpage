<?php

class Trabajo_Model extends CI_Model 
{
	public $id                        = '';
	public $nombre                    = '';
	public $fecha_creacion            = '';
	public $fecha_ultima_modificacion = '';	
	public $estado                    = '';
	public $empresa                   = '';	
	public $empresa_id                = '';	
	public $porcentaje                = '';	

	function crear($nombre, $empresa_id)
	{
		$fecha = date("Y-m-d");

		$this->db->query("INSERT INTO trabajos 
							SET 
								nombre                    = '$nombre',
								fecha_creacion            = '$fecha',
								fecha_ultima_modificacion = '$fecha',
								empresa_id                = $empresa_id
						");

		$this->id                        = $this->db->insert_id();
		$this->nombre                    = $nombre;
		$this->fecha_creacion            = $fecha;
		$this->fecha_ultima_modificacion = $fecha;	
		$this->estado                    = 'pendiente';	
		$this->empresa_id                = $empresa_id;
	}


	function cargar($id)
	{
		$query = $this->db->query("SELECT * FROM trabajos WHERE id = $id");

		$datos = $query->row();
		
		$this->id                        = $id;
		$this->nombre                    = $datos->nombre;
		$this->fecha_creacion            = $datos->fecha_creacion;		
		$this->fecha_ultima_modificacion = $datos->fecha_ultima_modificacion;
		$this->estado                    = $datos->estado;
		$this->empresa_id                = $datos->empresas_id;	

		$this->porcentaje = $this->calcular_porcentaje_realizado_trabajo($this->id);		
	}


	function listar($empresa)
	{
		if ($_SESSION['usuario_rol'] == 'admin') 
		{
			$query = $this->db->query("SELECT t.*, t.nombre as nombre, e.nombre as empresa 
										FROM trabajos t
										JOIN empresas e ON e.id = t.empresas_id");
		}
		else
		{
			$query = $this->db->query("SELECT t.*, t.nombre as nombre, e.nombre as empresa 
										FROM trabajos t
										JOIN empresas e ON e.id = t.empresas_id
										WHERE empresas_id = $empresa->id");
		}
		
		$trabajos = array();

		foreach ($query->result() as $trabajo)
		{
			$trabajos[] = array(
									"id"                        => $trabajo->id,
									"nombre"                    => $trabajo->nombre,
									"fecha_creacion"            => $trabajo->fecha_creacion,
									"fecha_ultima_modificacion" => $trabajo->fecha_ultima_modificacion,
									"estado"                    => $trabajo->estado,
									"empresa"                   => $trabajo->empresa,
									"empresa_id"                => $trabajo->empresas_id,
									"porcentaje"				=> $this->calcular_porcentaje_realizado_trabajo($trabajo->id)
								  );
		}

		return $trabajos;	
	}	


	function listar_actividades()
	{
		$query = $this->db->query("SELECT * FROM actividades WHERE trabajos_id = $this->id");

		$actividades = array();

		foreach ($query->result() as $actividad)
		{
			$actividades[] = array(
									"id"                        => $actividad->id,
									"nombre"                    => $actividad->nombre,
									"descripcion"                    => $actividad->descripcion,
									"fecha_creacion"            => $actividad->fecha_creacion,
									"fecha_ultima_modificacion" => $actividad->fecha_ultima_modificacion,
									"estado"                    => $actividad->estado,
									"porcentaje"				=> $this->calcular_porcentaje_realizado_actividad($actividad->id)
								  );
		}

		return $actividades;		
	}


	function calcular_porcentaje_realizado_trabajo($trabajo_id)
	{
		$query = $this->db->query("SELECT estado, porcentaje FROM actividades WHERE trabajos_id = $trabajo_id AND estado <> 'pendiente'");

		$num_actividades = $query->num_rows();

		if ($num_actividades == 0) 
		{
			return 100;
		}

		$porcentaje_acumulado = 0;

		$actividades = $query->result();	

		foreach ($actividades as $actividad) 
		{
			switch ($actividad->estado)
			{
				case 'terminado':
					$porcentaje_acumulado += $actividad->porcentaje/100;
					break;
				case 'pendiente':
					$num_actividades_realizadas += 0;
					break;			
				default: //incompleto, revision
					$porcentaje_acumulado += $actividad->porcentaje/100;
					break;						
			}
		}	

		return round($porcentaje_acumulado / $num_actividades * 100);
	}


	function calcular_porcentaje_realizado_actividad($actividad_id)
	{
		$query = $this->db->query("SELECT * FROM subactividades WHERE actividades_id = $actividad_id");

		$num_subactividades = $query->num_rows();

		if ($num_subactividades == 0) 
		{
			return 100;
		}

		$num_subactividades_realizadas = 0;

		$subactividades = $query->result();	

		foreach ($subactividades as $subactividad) 
		{
			switch ($subactividad->estado)
			{
				case 'terminado':
					$num_subactividades_realizadas += 1;
					break;
				case 'pendiente':
					$num_subactividades_realizadas += 0;
					break;			
				default:
					$num_subactividades_realizadas += 0;
					break;						
			}
		}	

		$porcentaje = round($num_subactividades_realizadas / $num_subactividades * 100);

		$query = $this->db->query("UPDATE actividades SET porcentaje = $porcentaje WHERE id = $actividad_id");

		return $porcentaje;
	}
}

?>