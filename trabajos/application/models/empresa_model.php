<?php

class Empresa_Model extends CI_Model 
{
	public $id                        = '';
	public $nombre                    = '';
	public $fecha_creacion            = '';

	function crear($nombre)
	{
		$fecha = date("Y-m-d");

		$this->db->query("INSERT INTO empresas 
							SET 
								nombre                    = '$nombre',
								fecha_creacion            = '$fecha',
						");

		$this->id             = $this->db->insert_id();
		$this->nombre         = $nombre;
		$this->fecha_creacion = $fecha_creacion;	
	}

	function cargar($id)
	{
		$query = $this->db->query("SELECT * FROM empresas WHERE id = $id");

		$datos = $query->row();
		
		$this->id             = $id;
		$this->nombre         = $datos->nombre;
		$this->fecha_creacion = $datos->fecha_creacion;				
	}

	function listar()
	{
		$query = $this->db->query("SELECT * FROM empresas");

		return $query->result();
	}	
}

?>