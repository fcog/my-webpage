<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('acceso'))
{
    function acceso()
    {
		if (!isset($_SESSION['usuario_id']))
		{
			redirect('ingreso');
		}
    }   		
}

?>