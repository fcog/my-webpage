<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller  
{

    function __construct()  
    {
       parent::__construct();
    }

    public function master_view($content_view, $data="")
    {
        $this->load->view('header', $data);
        $this->load->view($content_view, $data);
        $this->load->view('footer');
    }    
} 
?>